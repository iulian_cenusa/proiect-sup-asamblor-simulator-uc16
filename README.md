# Proiect SuP Asamblor/Simulator Uc16
Acest proiect isi propune sa creeze un asamblor/simulator pentru microprocesorul UC16.
Este realizat in limbajul Python . Pentru interfata grafica este utilizata libraria AppJar.

#Features implementate :
 - asamblor functional
 - simulator logic pentru and si or ( xor urmeaza )
 - simulator aritmetic pentru ad , sb , ml si dv
 - simulatorul logic produce un mesaj de exceptie dac aintalneste o instructiune aritmetica
 - simulatorul aritmetic produce un mesaj de exceptie dac aintalneste o instructiune logica
 - programul produce o eroare daca fisierul de intrare nu exista
 - UI simplu
 - creeaza un fisier de iesire pentru asamblare
 - error_flag care va produce mesaje de eroare daca ceva nu e in regula

#Features pe care vreau sa le implementez :
 - genereare de eroare daca introduc o instructiune care nu exista
 - simulator logic pentru xor
 - simulator aritmetic pentru adi si sbi
 

#Bug-uri identificate
 - la simulare aritmetica sau logica care contine o instructiune din cealalta , apare mai intai mesajul ca s-a asamblat corect , apoi mesajul de eroare ca avem instructiuni gresite

