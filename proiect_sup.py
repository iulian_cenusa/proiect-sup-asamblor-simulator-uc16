from appjar import gui #preluam modului gui din libraria appjar
#----------------------
#partea de initializare de registrii
#-----------------------------------
#dictionar pentru instructiuni
val = {
    "ad" :"0000" ,
    "sb" :"0001" ,
    "ml" :"0010",
    "dv" :"0011" ,
    "adi":"0100" ,
    "sbi":"0101" ,
    "rti":"0110",
    "sh" :"0111" ,
    "and":"1000",
    "or" :"1001" ,
    "xor":"1010" ,
    "msk":"1011" ,
    "ld" :"1100" ,
    "st" :"1101",
    #----brx---
    "bri":"1110",
    "brv":"1110",
    "brz":"1110",
    "brn":"1110",
    #----bix---
    "bii":"1111",
    "biv":"1111",
    "biz":"1111",
    "bin":"1111",
    #----------
    }
#-----------------------------------
#dictionar pentru registrii
reg = {
    "r0" :"0000",
    "r1" :"0001",
    "r2" :"0010",
    "r3" :"0011",
    "r4" :"0100",
    "r5" :"0101",
    "r6" :"0110",
    "r7" :"0111",
    "r8" :"1000",
    "r9" :"1001",
    "r10":"1010",
    "r11":"1011",
    "r12":"1100",
    "r13":"1101",
    "r14":"1110",
    "r15":"1111"   
    }
#------------------------------------
#dictionar pentru valorle aritmetice ale registrelor
arit ={
    "r0":0,
    "r1":1,
    "r2":2,
    "r3":3,
    "r4":4,
    "r5":5,
    "r6":6,
    "r7":7,
    "r8":8,
    "r9":9,
    "r10":10,
    "r11":11,
    "r12":12,
    "r13":13,
    "r14":14,
    "r15":15
    }
#dictionar pentru valorile logice ale registrelor
logic ={
    "r0":1,
    "r1":1,
    "r2":1,
    "r3":1,
    "r4":1,
    "r5":1,
    "r6":1,
    "r7":1,
    "r8":1,
    "r9":0,
    "r10":0,
    "r11":0,
    "r12":0,
    "r13":0,
    "r14":0,
    "r15":0
    }
#----------------------
error_flag = 0 # flag care daaca se face 1 va fi o eroare in instructiuni sau in program si nu va afisa nimic la iesire
#----------------------
def tran (lista): #functie car erealizeaza asamblarea
    error_flag = 0 # flag de eroare
    for i in range( len(lista) ): # se verifică fiecare element din listă
        if  i % 4 == 0 : # daca instructiunea aleasă este pe indicele 0,4,8,12.....
            if lista[i] in val: #daca exista valaorea in dictionarul cu instructiuni (numit val) 
                lista[i] = '\n' + str(val[str(lista[i])]) #se transforma instructiunea cu valoarea in biti corespunzatoare
            else:
                win.errorBox("eroare","Exceptie ! Fisierul contine instructiuni gresite !") #avem o eroare/exceptie
                error_flag = 1 # avem o eroare
                break #intrerupem executia for-ului
        if i% 4 != 0 :
            lista[i] = ' ' + str(reg[str(lista[i])]) #se modifica registrii cu valoarea in binar corespunzatoare
                    
#----------------------
def tran2(nume_fis): #functie pentru partea de simulare
    #-------------------
    error_flag = 0 # flag de eroare
    #-------------------
    f = open( nume_fis , "r" ) # deschidem fisierul de intrare
    lista=[] # declaram o lista goala in care sa punem textul extras din fisierul de intrare
    for line in f: #parcurgem linie cu linie
        for word in line.split(): #parcurgem cuvant cu cuvant
            lista.append(word) #adaugam la lista
    f.close() #inchidem fisierul de intrare
    #-------------------
    if lista[0] == "and" or lista[0] == "or" or lista[0] == "xor" : # verificam daca prima instructiune din fisier este logica
        #daca este logica atunci avem cazul in care fisierul de intrare este (sau ar trebuii sa fie) constituit doar din
        #instructiuni logice
        caz = 0 # logic
    else:
        #altfel instructiuni aritmetice
        caz = 1 # valori
    #-------------------
    if caz == 0 : #logic
        try: # facem try...catch pentru a capta eventualele erori
            for i in range (0 , len( lista ) ):
                if i % 4 == 0 :
                    if lista[i] == "and" :
                        logic[lista[i+1]] = logic[lista[i+2]] and logic[lista[i+3]]
                    if lista[i] == "or" :
                        logic[lista[i+1]] = logic[lista[i+2]] or logic[lista[i+3]]
                    if lista[i] == "xor" :
                        #(a and not b) or (not a and b) xor definition
                        pass # to be implemented
                    if lista[i] == "ad" or lista[i] == "sb" or lista[i] == "ml" or lista[i] == "dv" :
                        win.errorBox("eroare","Exceptie ! Instructiune aritmetica in lista logica !")
                        error_flag = 1 # avem o eroare
                        break
                    #functie neutilizata
                    #if val.has_key( str( lista[i] ) ) :
                    #    pass
                    #else:
                    #    win.errorBox("eroare","Exceptie ! Fisierul contine instructiuni gresite !")
                    #    error_flag = 1 # avem o eroare
                    #    break
            lista3=[] #initializam o noua lista
            for i in range(0,15):
                lista3.append("\nR"+str(i)+" = ")
                lista3.append(logic["r"+str(i)])
            #print ("\n") #pentru debugging
            #print ( lista3 ) #pentru debugging
            #------------------------------------------
        except :
            win.errorBox("eroare","Exceptie logic !")
        #if error_flag == 0:
        #   win.setLabel("p8"," ".join(str(x) for x in lista3))
    #-------------------
    if caz == 1 : #aritmetic
        try:
            for i in range (0 , len( lista ) ):
                if i%4 == 0 :
                    if lista[i] == "ad": #ad
                        arit[ lista[i+1] ] = arit[ lista[i+2] ] + arit[ lista[i+3] ]
                    if lista[i] == "sb": #sb
                        arit[ lista[i+1] ] = arit[ lista[i+2] ] - arit[ lista[i+3] ]
                    if lista[i] == "ml": #ml
                        arit[ lista[i+1] ] = arit[ lista[i+2] ] * arit[ lista[i+3] ]
                    if lista[i] == "dv": #dv
                        arit[ lista[i+1] ] = arit[ lista[i+2] ] / arit[ lista[i+3] ]
                    if lista[i] == "and" or lista[i] == "or" or lista[i] == "xor" : #daca avem instructiune logica
                        win.errorBox("eroare","Exceptie ! Instructiune logica in lista aritmetica !")
                        error_flag = 1 # avem o eroare
                        break
            lista3=[] #initializam o noua lista
            for i in range(0,15):
                lista3.append("\nR"+str(i)+" = ")
                lista3.append(arit["r"+str(i)])
            #print ("\n") #pentru debugging
            #print ( lista3 ) #pentru debugging
        except:
            win.errorBox("eroare","Exceptie aritmetic !")
    if error_flag == 0:
        win.setLabel("p8"," ".join(str(x) for x in lista3))
    #-------------------
#----------------------
win = gui("Proiect SuP v0.5") #titlul ferestrei
win.setGeometry("600x800") #dimensiunea in pixeli
win.setResizable("False") # daca vrem sau nu sa redimensionam fereastra
#----------------------
win.setBg("orange") #culoare de background
win.setFg("blue") #culoarea textului 
win.setFont(14) #fontul textului
#nota : aceste trei caracteristici se pot modifica pentru fiecare eticheta introdusa in program
#pentru simplitate voi pastra aceleasi caracteristici pentru toate elemetele programului
#----------------------
win.addLabel("p1","Proiect SuP v0.5",0,0,2) #eticheta
win.addLabel("p11","Asamblor/Simulator UC16",0,2,2)
#----------------------
win.addLabel("p3","Introduceti fisierul ce contine instructiuni UC16",3,0,4)
#----------------------
win.addLabelEntry("Intrare",4,0,4) #zona unde se poate introduce text
win.setFocus("Intrare") # se seteaza focusul pe zona de introdus text
nume_fis = win.getEntry( "Intrare" ) #variabila care preia textul introdus in campul cu id-ul "Intrare"
#si care va functiona ca numele fisierului de intrare din program
#----------------------
def apasa(nume): #functie asiciata butonului de asamblare
    #----------------------------------
    win.setLabel("p7","")
    #----------------------
    win.setLabel("p8","")
    #----------------------------------
    nume_fis = win.getEntry( "Intrare" )
    try:
        f = open( nume_fis , "r" )
        lista = f.readlines()
        f.close()
        f = open( nume_fis , "r" )
        lista2=[]
        for line in f:
            for word in line.split():
               lista2.append(word)
        win.infoBox("Succes","Asamblat cu success !!!")
        f.close()
        #print(lista,lista2)# pentru debugging
        tran(lista2)
        win.setLabel("p7","".join(str(x) for x in lista))
        if error_flag == 0:
            win.setLabel("p8","".join(str(x) for x in lista2))
            f = open( "out.txt","a" )
            f.write("\n")
            for i in range ( 0 , len( lista2 ) ):
                f.write(lista2[i] + " ")
            f.write("\n")
            f.close()
    except IOError:
        win.errorBox("eroare","Fisierul nu a putut fi asamblat ! Incercati cu un fisier valid !")
win.addButton( "Asamblare",  apasa )
#----------------------
def iesi(nume): #functia pentru butonul de exit
    win.stop() #exit
win.addButton("Exit",iesi,5,1,2) #buton de exit
def apasa2(nume): #functie asociata butonului de simulare
    #----------------------------------
    win.setLabel("p7","")
    #----------------------
    win.setLabel("p8","")
    #----------------------------------
    nume_fis = win.getEntry( "Intrare" ) # preluam ca nume a fisierului de intrare campul din campul cu id-ul "Intrare"
    try:
        f = open( nume_fis , "r" )
        lista = f.readlines() #lista 
        f.close()
        f = open( nume_fis , "r" )
        lista2=[] #lista  
        for line in f:
            for word in line.split():
               lista2.append(word)
        win.infoBox("Succes","Simulat cu success !!!")
        f.close()
        #print("\n")
        #print(lista2) # pentru debugging
        #print("\n")
        #lista3=[] nu mai folosesc
        tran2(nume_fis) #-----numele fisierului ca parametru-----
        win.setLabel("p7","".join(str(x) for x in lista))
        #win.setLabel("p8","".join(str(x) for x in lista3))
    except IOError: #daca avem eroare I/O
        win.errorBox("eroare","Fisierul nu a putut fi simulat ! Incercati cu un fisier valid !")
win.addButton("Simulare",apasa2,5,3)
#----------------------
#----alte etichete-----
win.addLabel("p5","Continut fisier intrare",6,0,2)
#----------------------
win.addLabel("p6","Continut fisier iesire",6,2,2)
#----------------------
win.addLabel("p7","",7,0,2)
#----------------------
win.addLabel("p8","",7,2,2)
#----------------------
win.addLabel("p2","Iulian Cenusa AIA 3231B",8,0,4)
#----------------------
win.go() #porneste interfata grafica
